<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label for="">First name : </label><br>
        <input type="text" name="firstname" id="firstname"><br>
        <label for="">Last name : </label><br>
        <input type="text" name="lastname" id="lastname"><br><br>
        <label for="">Gender</label><br>
        <input type="radio" name="gender" value="MALE" id="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="FEMALE" id="female">
        <label for="female">Female</label><br><br>
        <label for="nationality">Nationality</label>
        <select name="nationality" id="nationality"><br><br>
            <option value="INDONESIA">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
        </select><br><br>
        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" name="language" value="BINDO" id="bindo">
        <label for="bindo">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" value="ENG" id="eng">
        <label for="eng">English</label><br>
        <input type="checkbox" name="language" value="OTHER" id="other">
        <label for="other">Other</label><br><br>
        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="sign_up">
    </form>
</body>

</html>